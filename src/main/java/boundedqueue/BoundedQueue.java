package boundedqueue;

import java.util.ArrayList;
import java.util.List;

class BoundedQueue<T> {

    private int size=0;

    private List<T> elements;
    private List<T> buffer;

    public BoundedQueue(int size) {
        this.size = size;
        buffer = new ArrayList<>();
        elements = new ArrayList<>(size);
    }

    public int getSize() {
        return size;
    }

	public void enqueue(T element) {
        if(getCount()<size){
            elements.add(element);
        }
        else{
            buffer.add(element);
        }
	}

	public int getCount() {
		return elements.size();
	}

	public T dequeue() {
        T element=null;
        if(!elements.isEmpty()){
            element = elements.remove(0);
            if(!buffer.isEmpty()){
                elements.add(buffer.remove(0));
            }
        }
        return element;
	}   
}