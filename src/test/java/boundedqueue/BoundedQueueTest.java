package boundedqueue;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BoundedQueueTest{

    @Test
    public void BoundedQueueSizeTest(){
        int size = 2;
        BoundedQueue<Integer> bq = new BoundedQueue<Integer>(size);

        assertTrue(size==bq.getSize());
    }

    @Test
    public void EnqueueTest(){
        int size = 2;
        BoundedQueue<Integer> bq = new BoundedQueue<Integer>(size);

        bq.enqueue(1);

        assertTrue(1==bq.getCount());
    }

    @Test
    public void DequeueTest(){
        int size = 2;
        BoundedQueue<Integer> bq = new BoundedQueue<Integer>(size);
        Integer element = 1;
        bq.enqueue(element);

        Integer actual = bq.dequeue();

        assertTrue(element==actual);
    }

    @Test
    public void DequeueMultipleTest(){
        int size = 2;
        BoundedQueue<Integer> bq = new BoundedQueue<Integer>(size);
        bq.enqueue(2);
        bq.enqueue(3);

        assertTrue(2 == bq.dequeue());
    }

    @Test
    public void EnqueueBlockedTest(){
        int size = 2;
        BoundedQueue<Integer> bq = new BoundedQueue<Integer>(size);
        bq.enqueue(2);
        bq.enqueue(3);
        
        bq.enqueue(4); // blocked

        assertTrue(2 == bq.getCount());
        assertTrue(2 == bq.dequeue());
    }

    @Test
    public void EnqueueComeFreeTest(){
        int size = 2;
        BoundedQueue<Integer> bq = new BoundedQueue<Integer>(size);
        bq.enqueue(2);
        bq.enqueue(3);        
        bq.enqueue(4); // blocked
        bq.dequeue();

        assertTrue(2 == bq.getCount());
        assertTrue(3 == bq.dequeue());
        assertTrue(4 == bq.dequeue());
    }

    @Test
    public void DequeueBlockedTest(){
        int size = 2;
        BoundedQueue<Integer> bq = new BoundedQueue<Integer>(size);

        assertTrue(null == bq.dequeue());
    }
}